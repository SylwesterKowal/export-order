<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-06-16
 * Time: 01:20
 */

namespace Kowal\ExportOrder\Lib;


class CurlServices
{
    private $webApiUrl;
    private $requestTokenUrl = '/rest/V1/integration/admin/token/'; // for Magento
    private $dataString = [];
    private $requestType = "POST";
    private $adminName;
    private $adminPassword;
    private $accessToken;
    public $prefix = "";

    /**
     * https://kowal.co/index.php/rest
     *
     * @param $url
     * @return $this
     */
    public function setUrl($host, $protocol = 'https')
    {
        $this->webApiUrl = $protocol . '://' . $host;
        return $this;
    }

    /**
     * @param string $dataString
     * @return $this
     */
    public function setDataString($dataString = "")
    {
        $this->dataString = $dataString;
        return $this;
    }

    public function setRequestTokenUrl($dataString = "")
    {
        $this->requestTokenUrl = $dataString;
        return $this;
    }

    /**
     * @param string $requestType
     * @return $this
     */
    public function setRequest($requestType = "POST")
    {
        $this->requestType = $requestType;
        return $this;
    }

    /**
     * @param $adminName
     * @param $adminPassword
     * @return $this
     */
    public function setCreditials($adminPassword, $adminName = '')
    {
        $this->adminName = $adminName;
        $this->adminPassword = $adminPassword;
        $this->accessToken = $adminPassword;
        return $this;
    }

    public function exexute($resource, $array = false, $header = [])
    {
        if ($this->accessToken === null) {
            $this->requestToken();
        }


        $ch = curl_init($this->webApiUrl . $resource);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->requestType);
        if ($this->requestType != 'GET') {
//            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->dataString));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->dataString));
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $result = curl_exec($ch);

        $err = curl_error($ch);
        $errno = intval(curl_errno($ch));

        curl_close($ch);

        // sprawdzamy odpowiedz
        if (!$err) {
            $result_data = ($array) ? json_decode($result, true) : json_decode($result);
            return $result_data;
        } else {
            // problem z wykonaniem requestu
            var_dump($err, $errno);
            return false;
        }


    }


    /**
     * @return mixed
     * @throws \Exception
     */
    public function requestToken()
    {

        $this->accessToken = $this->adminPassword;
        return;

        $data = [
            "username" => $this->adminName,
            "password" => $this->adminPassword
        ];

        $ch = curl_init($this->webApiUrl . $this->requestTokenUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen(json_encode($data)))
        );
        $result = curl_exec($ch);
        curl_close($ch);
        $this->accessToken = json_decode($result);
    }

}
