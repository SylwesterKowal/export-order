<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 02/10/2021
 * Time: 01:49
 */

namespace Kowal\ExportOrder\Lib;

class FtpServices
{
    private $server;
    private $username;
    private $password;

    public function __construct($server, $username, $password)
    {
        $this->server = $server;
        $this->username = $username;
        $this->password = $password;
    }

    public function download($remote, $local = null)
    {
        if ($local === null) {
            $local = tempnam('/tmp', 'implicit_ftp');
        }

        if ($fp = fopen($local, 'w')) {
            $ftp_server = 'ftp://' . $this->server . '/' . $remote;
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $ftp_server);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERPWD, $this->username . ':' . $this->password);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);

            $xml = curl_exec($ch);
            if (curl_error($ch)) {
                var_dump(curl_error($ch));
                curl_close($ch);
            } else {
                curl_close($ch);
            }


            // save xml content on file
            fwrite($fp, $xml);
            fclose($fp);

            return $local;

        }
    }

    public function upload($local, $remote)
    {
        if ($fp = fopen($local, 'r')) {
            $ftp_server = 'ftp://' . $this->server . '/' . $remote;
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $ftp_server);
            curl_setopt($ch, CURLOPT_USERPWD, $this->username . ':' . $this->password);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
//            curl_setopt($ch, CURLOPT_FTP_SSL, CURLFTPSSL_TRY);
            curl_setopt($ch, CURLOPT_FTPSSLAUTH, CURLFTPAUTH_TLS);
            curl_setopt($ch, CURLOPT_UPLOAD, 1);
            curl_setopt($ch, CURLOPT_INFILE, $fp);

            curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);

            return !$err;
        }
        return false;
    }

}
