<?php
/**
 *
 */
namespace Kowal\ExportOrder\Lib\Template;

abstract class Base extends \Kowal\ExportOrder\Lib\Seller
{

    public function checkProductsToORder($orderItems, $prefix = "schemat_")
    {
        $count = 0;
        foreach ($orderItems as $item) {
            if ($item->getParentItemId()) {
                continue;
            }


            if (strpos($item->getSku(), $prefix) !== false) {
                if ($item->getExported() == "1") continue;
                $count++;
            } else {
                continue;
            }
        }
        return ($count > 0) ? true : false;
    }
}
