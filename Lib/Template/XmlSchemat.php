<?php

namespace Kowal\ExportOrder\Lib\Template;
class XmlSchemat extends \Kowal\ExportOrder\Lib\Template\Base
{
    private $order = null;
    public $seller = null;
    public $directoryList = null;
    public $file = null;

    public function __construct(
        $order,
        $test = null,
        \Magento\Framework\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Filesystem\Io\File $file,
        \Kowal\ExportOrder\Lib\MagentoService $magentoService,
        \Kowal\ExportOrder\Lib\CurlServices $curlServices
    )
    {
        $this->order = $order;
        $this->directoryList = $directoryList;
        $this->file = $file;
        $this->magentoService = $magentoService;
    }

    public function execute()
    {
        $orderItems = $this->order->getAllItems();
        // sprawdzamy czy są produkty do zamowienia z hurtowni schemat
        if (!$this->checkProductsToORder($orderItems, "schemat_")) return "Pominięte " . $this->order->getIncrementId();

        $payment = $this->order->getPayment();
        $method = $payment->getMethodInstance();
        $paymentTitle = $method->getTitle();
        $paymentTitle = $method->getCode();


        $shippingAddress = $this->order->getShippingAddress();
        $person = trim($shippingAddress->getFirstname() . ' ' . $shippingAddress->getLastname());
        $company = (!empty($shippingAddress->getCompany())) ? trim($shippingAddress->getCompany()) : "";
        $street = $shippingAddress->getStreet()[0];
        $street = (isset($shippingAddress->getStreet()[1])) ? $street . ' ' . $shippingAddress->getStreet()[1] : $street;
        $vat_id = $shippingAddress->getVatId();

        $billingAddress = $this->order->getBillingAddress();
        $blling_person = trim($billingAddress->getFirstname() . ' ' . $billingAddress->getLastname());
        $blling_company = (!empty($billingAddress->getCompany())) ? trim($billingAddress->getCompany()) : "";
        $blling_street = $billingAddress->getStreet()[0];
        $blling_street = (isset($billingAddress->getStreet()[1])) ? $blling_street . ' ' . $billingAddress->getStreet()[1] : $blling_street;
//            file_put_contents("_order_billing_data.txt",print_r($billingAddress->getData(),true));


        $xml = "<Document-Order>";
        $xml .= "<Order-Header>";
        $xml .= "<OrderNumber>{$this->order->getIncrementId()}</OrderNumber>";
        $xml .= "<VendorOrderNumber>{$this->order->getIncrementId()}</VendorOrderNumber>";
        $xml .= "<OrderDate>{$this->getDate($this->order->getCreatedAtFormatted(3))}</OrderDate>";
        $xml .= "<OrderTime>{$this->getTime($this->order->getCreatedAtFormatted(3))}</OrderTime>";
        $xml .= "<ExpectedDeliveryDate>{$this->getDate($this->order->getCreatedAtFormatted(3))}</ExpectedDeliveryDate>";
        $xml .= "<ExpectedDeliveryTime>{$this->getTime($this->order->getCreatedAtFormatted(3))}</ExpectedDeliveryTime>";
        $xml .= "<LatestDeliveryDate>{$this->getDate($this->order->getCreatedAtFormatted(3))}</LatestDeliveryDate>";
        $xml .= "<LatestDeliveryTime>{$this->getTime($this->order->getCreatedAtFormatted(3))}</LatestDeliveryTime>";
        $xml .= "<EarliestDeliveryDate>{$this->getDate($this->order->getCreatedAtFormatted(3))}</EarliestDeliveryDate>";
        $xml .= "<EarliestDeliveryTime>{$this->getTime($this->order->getCreatedAtFormatted(3))}</EarliestDeliveryTime>";
        $xml .= "<CollectionDate>{$this->getDate($this->order->getCreatedAtFormatted(3))}</CollectionDate>";
        $xml .= "<CollectionTime>{$this->getTime($this->order->getCreatedAtFormatted(3))}</CollectionTime>";
        $xml .= "<ContractNumber></ContractNumber>";
        $xml .= "<ContractDate></ContractDate>";
        $xml .= "<DocumentExpirationDate></DocumentExpirationDate>";
        $xml .= "<PromotionReference></PromotionReference>";
        $xml .= "<DocumentFunctionCode>O</DocumentFunctionCode>";
        $xml .= "<DocumentFunctionCode>OD</DocumentFunctionCode>";
        $xml .= "<Remarks></Remarks>";
        $xml .= "<OrderCurrency>PLN</OrderCurrency>";
        $xml .= "<Payment>>";
        $xml .= "<PaymentTerms>";
        $xml .= "<PaymentTermsType>1</PaymentTermsType>";
        $xml .= "<PaymentMeans>42</PaymentMeans>";
        $xml .= "<PaymentDescription></PaymentDescription>";
        $xml .= "<Percentage></Percentage>";
        $xml .= "<PeriodType>D</PeriodType>";
        $xml .= "<PeriodsNumber>2</PeriodsNumber>";
        $xml .= "<PaymentDate></PaymentDate>";
        $xml .= "</PaymentTerms>";
        $xml .= "</Payment>";
        $xml .= "<Delivery>";
        $xml .= "<DeliveryTerms>";
        $xml .= "<DeliveryTermsCode>CPT</DeliveryTermsCode>";
        $xml .= "<DeliveryTermsDescription></DeliveryTermsDescription>";
        $xml .= "<PaymentMethod>PP</PaymentMethod>";
        $xml .= "</DeliveryTerms>";
        $xml .= "</Delivery>";
        $xml .= "</Order-Header>";
        $xml .= "<Order-Parties>";
        $xml .= "<Buyer>";
        $xml .= "<ILN>{$this->id}</ILN>";
        $xml .= "<TaxID>{$this->vat_id}</TaxID>";
        $xml .= "<CodeByBuyer>{$this->codeByBuyer}</CodeByBuyer>";
        $xml .= "<AccountNumber>{$this->accountNumber}</AccountNumber>";
        $xml .= "<Name>{$this->name}</Name>";
        $xml .= "<StreetAndNumber>{$this->streetAndNumber}</StreetAndNumber>";
        $xml .= "<CityName>{$this->cityName}</CityName>";
        $xml .= "<PostalCode>{$this->postalCode}</PostalCode>";
        $xml .= "<Country>{$this->country}</Country>";
        $xml .= "<CourtAndCapitalInformation>{$this->courtAndCapitalInformation}</CourtAndCapitalInformation>";
        $xml .= "<ContactInformation>{$this->contactInformation}</ContactInformation>";
        $xml .= "<ContactPerson>{$this->contactPerson}</ContactPerson>";
        $xml .= "<PhoneNumber>{$this->phoneNumbern}</PhoneNumber>";
        $xml .= "<Fax>{$this->fax}</Fax>";
        $xml .= "<ElectronicMail>{$this->electronicMail}</ElectronicMail>";
        $xml .= "</Buyer>";
        $xml .= "<BuyerHeadquarters>";
        $xml .= "<ILN>{$this->id}</ILN>";
        $xml .= "<Name>{$this->name}</Name>";
        $xml .= "<StreetAndNumber>{$this->streetAndNumber}</StreetAndNumber>";
        $xml .= "<CityName>{$this->cityName}</CityName>";
        $xml .= "<PostalCode>{$this->postalCode}</PostalCode>";
        $xml .= "<Country>{$this->country}</Country>";
        $xml .= "<ContactInformation>{$this->contactInformation}</ContactInformation>";
        $xml .= "<ContactPerson>{$this->contactPerson}</ContactPerson>";
        $xml .= "<PhoneNumber>{$this->phoneNumbern}</PhoneNumber>";
        $xml .= "<Fax>{$this->fax}</Fax>";
        $xml .= "<ElectronicMail>{$this->electronicMail}</ElectronicMail>";
        $xml .= "</BuyerHeadquarters>";
        $xml .= "<Invoicee>";
        $xml .= "<ILN>{$this->id}</ILN>";
        $xml .= "<TaxID>{$this->vat_id}</TaxID>";
        $xml .= "<Name>{$this->name}</Name>";
        $xml .= "<StreetAndNumber>{$this->streetAndNumber}</StreetAndNumber>";
        $xml .= "<CityName>{$this->cityName}</CityName>";
        $xml .= "<PostalCode>{$this->postalCode}</PostalCode>";
        $xml .= "<Country>{$this->country}</Country>";
        $xml .= "<ContactInformation>{$this->contactInformation}</ContactInformation>";
        $xml .= "<ContactPerson>{$this->contactPerson}</ContactPerson>";
        $xml .= "<PhoneNumber>{$this->phoneNumbern}</PhoneNumber>";
        $xml .= "<Fax>{$this->fax}</Fax>";
        $xml .= "<ElectronicMail>{$this->electronicMail}</ElectronicMail>";
        $xml .= "</Invoicee>";
        $xml .= "<Seller>";
        $xml .= "<ILN>{$this->id}</ILN>";
        $xml .= "<TaxID>{$this->vat_id}</TaxID>";
        $xml .= "<CodeByBuyer>{$this->codeByBuyer}</CodeByBuyer>";
        $xml .= "<AccountNumber>{$this->accountNumber}</AccountNumber>";
        $xml .= "<Name>{$this->name}</Name>";
        $xml .= "<StreetAndNumber>{$this->streetAndNumber}</StreetAndNumber>";
        $xml .= "<CityName>{$this->cityName}</CityName>";
        $xml .= "<PostalCode>{$this->postalCode}</PostalCode>";
        $xml .= "<Country>{$this->country}</Country>";
        $xml .= "<CourtAndCapitalInformation>{$this->courtAndCapitalInformation}</CourtAndCapitalInformation>";
        $xml .= "<ContactInformation>{$this->contactInformation}</ContactInformation>";
        $xml .= "<ContactPerson>{$this->contactPerson}</ContactPerson>";
        $xml .= "<PhoneNumber>{$this->phoneNumbern}</PhoneNumber>";
        $xml .= "<Fax>{$this->fax}</Fax>";
        $xml .= "<ElectronicMail>{$this->electronicMail}</ElectronicMail>";
        $xml .= "</Seller>";
        $xml .= "<DeliveryPoint>";
        $xml .= "<ILN></ILN>";
        $xml .= "<TaxID>{$vat_id}</TaxID>";
        $xml .= "<CodeByBuyer></CodeByBuyer>";
        $xml .= "<Name>{$person}</Name>";
        $xml .= "<StreetAndNumber>{$street}</StreetAndNumber>";
        $xml .= "<CityName>{$shippingAddress->getCity()}</CityName>";
        $xml .= "<PostalCode>{$shippingAddress->getPostcode()}</PostalCode>";
        $xml .= "<Country>{$shippingAddress->getCountryId()}</Country>";
        $xml .= "<ContactInformation>{$person} {$company}</ContactInformation>";
        $xml .= "<ContactPerson>{$person}</ContactPerson>";
        $xml .= "<PhoneNumber>{$shippingAddress->getTelephone()}</PhoneNumber>";
        $xml .= "<Fax></Fax>";
        $xml .= "<ElectronicMail>{$this->order->getCustomerEmail()}</ElectronicMail>";
        $xml .= "</DeliveryPoint>";
        $xml .= "<ShipFrom>";
        $xml .= "<ILN></ILN>";
        $xml .= "<Name></Name>";
        $xml .= "<StreetAndNumber></StreetAndNumber>";
        $xml .= "<CityName></CityName>";
        $xml .= "<PostalCode></PostalCode>";
        $xml .= "<Country></Country>";
        $xml .= "<ContactInformation></ContactInformation>";
        $xml .= "<ContactPerson></ContactPerson>";
        $xml .= "<PhoneNumber></PhoneNumber>";
        $xml .= "<Fax></Fax>";
        $xml .= "<ElectronicMail></ElectronicMail>";
        $xml .= "</ShipFrom>";
        $xml .= "<OrderedBy>";
        $xml .= "<ILN>{$this->id}</ILN>";
        $xml .= "<TaxID>{$this->vat_id}</TaxID>";
        $xml .= "<AccountNumber>{$this->accountNumber}</AccountNumber>";
        $xml .= "<Name>{$this->name}</Name>";
        $xml .= "<StreetAndNumber>{$this->streetAndNumber}</StreetAndNumber>";
        $xml .= "<CityName>{$this->cityName}</CityName>";
        $xml .= "<PostalCode>{$this->postalCode}</PostalCode>";
        $xml .= "<Country>{$this->country}</Country>";
        $xml .= "<ContactInformation>{$this->contactInformation}</ContactInformation>";
        $xml .= "<ContactPerson>{$this->contactPerson}</ContactPerson>";
        $xml .= "<PhoneNumber>{$this->phoneNumbern}</PhoneNumber>";
        $xml .= "<Fax>{$this->fax}</Fax>";
        $xml .= "<ElectronicMail>{$this->electronicMail}</ElectronicMail>";
        $xml .= "</OrderedBy>";
        $xml .= "<Sender>";
        $xml .= "<ILN>{$this->id}</ILN>";
        $xml .= "<TaxID>{$this->vat_id}</TaxID>";
        $xml .= "<Name>{$this->name}</Name>";
        $xml .= "<StreetAndNumber>{$this->streetAndNumber}</StreetAndNumber>";
        $xml .= "<CityName>{$this->cityName}</CityName>";
        $xml .= "<PostalCode>{$this->postalCode}</PostalCode>";
        $xml .= "<Country>{$this->country}</Country>";
        $xml .= "<ContactInformation>{$this->contactInformation}</ContactInformation>";
        $xml .= "<ContactPerson>{$this->contactPerson}</ContactPerson>";
        $xml .= "<PhoneNumber>{$this->phoneNumbern}</PhoneNumber>";
        $xml .= "<Fax>{$this->fax}</Fax>";
        $xml .= "<ElectronicMail>{$this->electronicMail}</ElectronicMail>";
        $xml .= "</Sender>";
        $xml .= "<Receiver>";
        $xml .= "<ILN></ILN>";
        $xml .= "<TaxID>{$vat_id}</TaxID>";
        $xml .= "<Name>{$person}</Name>";
        $xml .= "<StreetAndNumber>{$street}</StreetAndNumber>";
        $xml .= "<CityName>{$shippingAddress->getCity()}</CityName>";
        $xml .= "<PostalCode>{$shippingAddress->getPostcode()}</PostalCode>";
        $xml .= "<Country>{$shippingAddress->getCountryId()}</Country>";
        $xml .= "<ContactInformation>{$person} {$company}</ContactInformation>";
        $xml .= "<ContactPerson>{$person}</ContactPerson>";
        $xml .= "<PhoneNumber>{$shippingAddress->getTelephone()}</PhoneNumber>";
        $xml .= "<Fax></Fax>";
        $xml .= "<ElectronicMail>{$this->order->getCustomerEmail()}</ElectronicMail>";
        $xml .= "</Receiver>";
        $xml .= "</Order-Parties>";

        $xml .= "<Order-Lines>";

        $order_item_increment = 0;
        $totla_qty = 0;
        $exported = [];
        foreach ($orderItems as $item) {
            if ($item->getParentItemId()) {
                continue;
            }

            if (strpos($item->getSku(), "schemat_") !== false) {
                $sku = str_replace("schemat_", "", $item->getSku());
                $ean = $this->magentoService->getAttribute($item->getSku(), 'ean13');
                $exported[] = $item->getItemId();
            } else {
                continue;
            }

            $totla_qty = $totla_qty + $item->getQtyOrdered();
            $order_item_increment++;

            $xml .= "<Line>";
            $xml .= "<Line-Item>";
            $xml .= "<LineNumber>{$order_item_increment}</LineNumber>";
            $xml .= "<SubLineNumber>{$order_item_increment}</SubLineNumber>";
            $xml .= "<BuyerLineNumber>{$order_item_increment}</BuyerLineNumber>";
            $xml .= "<EAN>{$ean}</EAN>";
            $xml .= "<BuyerItemCode>{$sku}</BuyerItemCode>";
            $xml .= "<SupplierItemCode>{$sku}</SupplierItemCode>";
            $xml .= "<PackageEAN></PackageEAN>";
            $xml .= "<ItemDescription>{$item->getName()}</ItemDescription>";
            $xml .= "<ItemType>CU</ItemType>";
            $xml .= "<OrderedQuantity>{$item->getQtyOrdered()}</OrderedQuantity>";
            $xml .= "<FreeOrderedQuantity></FreeOrderedQuantity>";
            $xml .= "<OrderedUnitPacksize>1</OrderedUnitPacksize>";
            $xml .= "<InvoicedQuantity>{$item->getQtyInvoiced()}</InvoicedQuantity>";
            $xml .= "<UnitOfMeasure>PCE</UnitOfMeasure>";
            $xml .= "</Line-Item>";
            $xml .= "<Line-Parties>";
            $xml .= "<DeliveryPoint>";
            $xml .= "<ILN></ILN>";
            $xml .= "<TaxID>{$vat_id}</TaxID>";
            $xml .= "<CodeByBuyer></CodeByBuyer>";
            $xml .= "<Name>{$person}</Name>";
            $xml .= "<StreetAndNumber>{$street}</StreetAndNumber>";
            $xml .= "<CityName>{$shippingAddress->getCity()}</CityName>";
            $xml .= "<PostalCode>{$shippingAddress->getPostcode()}</PostalCode>";
            $xml .= "<Country>{$shippingAddress->getCountryId()}</Country>";
            $xml .= "<ContactInformation>{$person} {$company}</ContactInformation>";
            $xml .= "<ContactPerson>{$person}</ContactPerson>";
            $xml .= "<PhoneNumber>{$shippingAddress->getTelephone()}</PhoneNumber>";
            $xml .= "<Fax></Fax>";
            $xml .= "<ElectronicMail>{$this->order->getCustomerEmail()}</ElectronicMail>";
            $xml .= "</DeliveryPoint>";
            $xml .= "</Line-Parties>";
            $xml .= "</Line>";

        }


        $xml .= "</Order-Lines>";
        $xml .= "<Order-Summary>";
        $xml .= "<TotalLines>{$order_item_increment}</TotalLines>";
        $xml .= "<TotalOrderedAmount>{$totla_qty}</TotalOrderedAmount>";
        $xml .= "</Order-Summary>";
        $xml .= "</Document-Order>";


        $xml_ = new \SimpleXMLElement($xml);

        $filename = 'order_schemat_' . $this->order->getIncrementId() . '.xml';
        $file_path_local = $this->getFileName($filename);
        $xml_->saveXML($file_path_local);

        $ftp = new \Kowal\ExportOrder\Lib\FtpServices("ftp.schemat.pl", "schemat_nirko", "SCHnir21!!");
        if ($result = $ftp->upload($file_path_local, "zamowienia/" . $filename)) {

            $this->magentoService->setExported($exported);
            // Zmień status atrybutu exported na 1 aby ponownie nie eksportowac zamowienia
            return $file_path_local;
        } else {
            return "Błąd w wysyłce: " . $file_path_local . $result;
        }


    }

    /**
     * 21.10.2021, 19:00
     * @param $date
     */
    private function getDate($date)
    {
        $a = explode(",", $date);
        $d = explode(".", $a[0]);
        return $d[2] . '-' . $d[1] . '-' . $d[0];
    }

    private function getTime($date)
    {
        $a = explode(",", $date);
        return trim($a[1]);
    }

    private function getFileName($filename)
    {
        $this->var = $this->directoryList->getPath('var');

        if (!file_exists($this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'orders')) {
            $this->file->mkdir($this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'orders', 0775);
        }
        return $this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'orders' . DIRECTORY_SEPARATOR . $filename;
    }



}
