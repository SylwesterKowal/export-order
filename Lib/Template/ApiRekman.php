<?php

namespace Kowal\ExportOrder\Lib\Template;
class ApiRekman extends \Kowal\ExportOrder\Lib\Template\Base
{
    private $test = 1; // Cy żądanie jest testowe? 1- tak, testowe, 2 - nie, produkcyjne.
    private $apiHost = 'https://api-json.rekman.com.pl';
    private $api_key_pub = '92ddb4c5261f4073484ffee01b32c11ac867fd1b26585d195e6ace7c589d844b';
    private $api_key_priv = '886d0412115ba5b3abfbedca28bce525a52997d20d2aa6c6160ad183d88efa2d';
    private $username = '';
    private $password = '';
    private $order = null;
    public $seller = null;
    public $directoryList = null;
    public $file = null;
    public $curlServices = null;


    public function __construct(
        $order,
        $test = null,
        \Magento\Framework\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Filesystem\Io\File $file,
        \Kowal\ExportOrder\Lib\MagentoService $magentoService,
        \Kowal\ExportOrder\Lib\CurlServices $curlServices
    )
    {
        $this->order = $order;
        $this->directoryList = $directoryList;
        $this->file = $file;
        $this->magentoService = $magentoService;
        $this->curlServices = $curlServices;
    }

    public function execute()
    {
        // Dokumentacja API: https://api-json.rekman.com.pl/

        $orderItems = $this->order->getAllItems();
        // sprawdzamy czy są produkty do zamowienia z hurtowni schemat
        if (!$this->checkProductsToORder($orderItems, "rekman_")) return "Pominięte " . $this->order->getIncrementId();

        $payment = $this->order->getPayment();
        $method = $payment->getMethodInstance();
        $paymentTitle = $method->getTitle();
        $paymentTitle = $method->getCode();

        $rekman_order_id = $this->addOrder($orderItems);
        return $this->order->getIncrementId() . ' -> ' . $rekman_order_id;

    }

    private function addAdresWysylkowy($blling_person, $blling_street, $shippingAddress, $url = '/pl/v1/order/new-ship-addr')
    {
        $this->data = [
            'addr_name' => $blling_person,
            'addr_line1' => $blling_street,
            'addr_line2' => '',
            'addr_line3' => '',
            'addr_zip' => $shippingAddress->getPostcode(),
            'addr_city' => $shippingAddress->getCity(),
            'addr_contact' => $blling_person,
            'addr_contact_phone' => $shippingAddress->getTelephone(),
            'email' => $this->order->getCustomerEmail(),
            'test' => $this->test
        ];
//        $accessToken = $this->curlServices->requestToken();
        // parametry uwierzytelniajace

        $api_nonce = time() . '2';            // to powoduje powstanie limitu 1 żądania na 1 sekundę
        // generujemy podpis
        $signature_base = "{$this->api_key_pub};{$this->apiHost}{$url};" . http_build_query($this->data) . ";{$api_nonce}";
        $api_signature = hash_hmac('sha256', $signature_base, $this->api_key_priv);

        $result = $this->curlServices
            ->setRequestTokenUrl('')
            ->setUrl(parse_url($this->apiHost, PHP_URL_HOST), parse_url($this->apiHost, PHP_URL_SCHEME))
            ->setCreditials($this->password, $this->username)
            ->setRequest('POST')
            ->setDataString($this->data)
//            ->exexute($url, true, ["Content-Type: application/json", "Authorization: Bearer " . $accessToken]);
            ->exexute($url, true,
                [
                    "API-key: {$this->api_key_pub}",
                    "API-signature: {$api_signature}",
                    "API-nonce: {$api_nonce}"
                ]
            );
        if ($result && isset($result['new-addr'])) {
            return $result['new-addr']['ticket_id'];
        } else {
            throw new \Exception(print_r($result, true) . print_r($this->data, true));
        }

    }

    private function addOrder($orderItems, $url = '/pl/v1/order/new')
    {
        $billingAddress = $this->order->getBillingAddress();
        $blling_person = trim($billingAddress->getFirstname() . ' ' . $billingAddress->getLastname());
        $blling_company = (!empty($billingAddress->getCompany())) ? trim($billingAddress->getCompany()) : "";
        $blling_street = $billingAddress->getStreet()[0];
        $blling_street = (isset($billingAddress->getStreet()[1])) ? $blling_street . ' ' . $billingAddress->getStreet()[1] : $blling_street;

        $shippingAddress = $this->order->getShippingAddress();
        $person = trim($shippingAddress->getFirstname() . ' ' . $shippingAddress->getLastname());
        $company = (!empty($shippingAddress->getCompany())) ? trim($shippingAddress->getCompany()) : "";
        $street = $shippingAddress->getStreet()[0];
        $street = (isset($shippingAddress->getStreet()[1])) ? $street . ' ' . $shippingAddress->getStreet()[1] : $street;
        $vat_id = $shippingAddress->getVatId();


        $skus = $this->getProductsPrices($orderItems);
        $set_as_exported = [];
        $exported = [];
        foreach ($orderItems as $item) {

            if ($item->getParentItemId()) {
                continue;
            }

            if (strpos($item->getSku(), "rekman_") !== false) {
                $sku = str_replace("rekman_", "", $item->getSku());

                $price_net = $skus[$sku];
                $qty = round($item->getQtyOrdered(), 0);
                $exported[] = "{$sku};{$qty};{$price_net}";
                $set_as_exported[] = $item->getItemId();
            } else {
                continue;
            }
        }

        $data = [
            'ship_ticket' => $adresWysyłkowyID = $this->addAdresWysylkowy($person, $street, $shippingAddress),
            'pay' => "DH-F-ZAL", //$this->getPayment(),
            'test' => $this->test,
            'concat' => 1,
            'own_nbr' => $this->order->getIncrementId(),
            'contact' => false,
            'contents' => $exported, //implode("&", $exported)
            'cmmt' => ''
        ];

        $api_nonce = time() . '3';            // to powoduje powstanie limitu 1 żądania na 1 sekundę
        // generujemy podpis
        $signature_base = "{$this->api_key_pub};{$this->apiHost}{$url};" . http_build_query($data) . ";{$api_nonce}";
        $api_signature = hash_hmac('sha256', $signature_base, $this->api_key_priv);

        $result = $this->curlServices
            ->setRequestTokenUrl('')
            ->setUrl(parse_url($this->apiHost, PHP_URL_HOST), parse_url($this->apiHost, PHP_URL_SCHEME))
            ->setCreditials($this->password, $this->username)
            ->setRequest('POST')
            ->setDataString($data)
//            ->exexute($url, true, ["Content-Type: application/json", "Authorization: Bearer " . $accessToken]);
            ->exexute($url, true,
                [
                    "API-key: {$this->api_key_pub}",
                    "API-signature: {$api_signature}",
                    "API-nonce: {$api_nonce}"
                ]
            );
        if ($result && isset($result['order_id'])) {
            $this->magentoService->setExported($set_as_exported);
            return $result['order_id'];
        } else {
            throw new \Exception(print_r($result, true) . print_r($data, true));
        }

    }


    private function getProductsPrices($orderItems, $prefix = 'rekman_', $url = '/pl/v1/catalog/prices-stocks')
    {
        $data = ['use_ppn' => 1];
        $skus = [];
        foreach ($orderItems as $item) {

            if ($item->getParentItemId()) {
                continue;
            }

            if (strpos($item->getSku(), $prefix) !== false) {
                $sku = str_replace($prefix, "", $item->getSku());
                $skus[] = $sku;
            } else {
                continue;
            }
        }
        $url = $url . "/" . implode(",", $skus);
        $api_nonce = time() . '1';            // to powoduje powstanie limitu 1 żądania na 1 sekundę
        // generujemy podpis
        $signature_base = "{$this->api_key_pub};{$this->apiHost}{$url};" . http_build_query($data) . ";{$api_nonce}";
        $api_signature = hash_hmac('sha256', $signature_base, $this->api_key_priv);

        $result = $this->curlServices
            ->setRequestTokenUrl('')
            ->setUrl(parse_url($this->apiHost, PHP_URL_HOST), parse_url($this->apiHost, PHP_URL_SCHEME))
            ->setCreditials($this->password, $this->username)
            ->setRequest('POST')
            ->setDataString($data)
//            ->exexute($url, true, ["Content-Type: application/json", "Authorization: Bearer " . $accessToken]);
            ->exexute($url, true,
                [
                    "API-key: {$this->api_key_pub}",
                    "API-signature: {$api_signature}",
                    "API-nonce: {$api_nonce}"
                ]
            );
        if ($result && isset($result['products'])) {
            $skus = [];
            foreach ($result['products'] as $product) {
                $skus[$product['ppn']] = $product['price']['net'];
            }
            return $skus;
        } else {
            throw new \Exception(print_r($result, true) . print_r($data, true) . ' ' . $url);
        }
    }

    private function getPayment($url = '/pl/v1/order/pay-terms')
    {
        $data = [];
        $api_nonce = time() . '4';            // to powoduje powstanie limitu 1 żądania na 1 sekundę
        // generujemy podpis
        $signature_base = "{$this->api_key_pub};{$this->apiHost}{$url};" . http_build_query($data) . ";{$api_nonce}";
        $api_signature = hash_hmac('sha256', $signature_base, $this->api_key_priv);

        $result = $this->curlServices
            ->setRequestTokenUrl('')
            ->setUrl(parse_url($this->apiHost, PHP_URL_HOST), parse_url($this->apiHost, PHP_URL_SCHEME))
            ->setCreditials($this->password, $this->username)
            ->setRequest('POST')
            ->setDataString($data)
//            ->exexute($url, true, ["Content-Type: application/json", "Authorization: Bearer " . $accessToken]);
            ->exexute($url, true,
                [
                    "API-key: {$this->api_key_pub}",
                    "API-signature: {$api_signature}",
                    "API-nonce: {$api_nonce}"
                ]
            );
        if ($result && isset($result['pay_terms'])) {
            var_dump($result);
            return $result['pay_terms'][0];
        } else {
            throw new \Exception(print_r($result, true));
        }
    }
}
