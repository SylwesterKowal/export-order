<?php

namespace Kowal\ExportOrder\Lib;

class Template
{
    private $order = null;
    private $theme = null;
    private $type = null;
    private $directoryList = null;
    private $file = null;
    private $magentoService = null;
    private $curlServices = null;
    private $test = null;

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function getContent()
    {
        $name = '\Kowal\ExportOrder\Lib\Template\\' . ucfirst($this->type) . ucfirst($this->theme);
        $class = new $name($this->order, $this->test, $this->directoryList, $this->file, $this->magentoService, $this->curlServices);

        return $class->execute();
    }

}
