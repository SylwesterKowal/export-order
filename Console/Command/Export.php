<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\ExportOrder\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class Export extends Command
{


    const TYPE = "type";
    const THEME = "Template";
    const STATUS = "status";
    const ID = "id";
    const TEST = "option";


    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        \Magento\Framework\Filesystem\DirectoryList                $directoryList,
        \Magento\Framework\Filesystem\Io\File                      $file,
        \Magento\Directory\Model\CountryFactory                    $countryFactory,
        \Kowal\ExportOrder\Lib\Template                            $template,
        \Kowal\ExportOrder\Lib\MagentoService                      $magentoService,
        \Kowal\ExportOrder\Lib\CurlServices                        $curlServices
    )
    {

        $this->collectionFactory = $collectionFactory;
        $this->directoryList = $directoryList;
        $this->file = $file;
        $this->countryFactory = $countryFactory;
        $this->template = $template;
        $this->magentoService = $magentoService;
        $this->curlServices = $curlServices;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        $type = $input->getArgument(self::TYPE);
        $theme = $input->getArgument(self::THEME);
        $status = $input->getArgument(self::STATUS);
        $order_id = $input->getArgument(self::ID);
        $option = $input->getOption(self::TEST);

        $collection = $this->getSalesOrderCollection(['status' => ['eq' => $status]]);
        foreach ($collection as $order) {
            $this->template->__set('order', $order);
            $this->template->__set('type', $type);
            $this->template->__set('theme', $theme);
            $this->template->__set('directoryList', $this->directoryList);
            $this->template->__set('file', $this->file);
            $this->template->__set('magentoService', $this->magentoService);
            $this->template->__set('curlServices', $this->curlServices);
            $this->template->__set('test', $option);
            $content = $this->template->getContent();

            $output->writeln("Order: " . $content);
        }

    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_exportorder:export");
        $this->setDescription("Export Orders [source]");
        $this->setDefinition([
            new InputArgument(self::TYPE, InputArgument::OPTIONAL, "Type [api,xml,json,csv,txt]"),
            new InputArgument(self::THEME, InputArgument::OPTIONAL, "Theme [leker,rekman,schemat,semma,pmi]"),
            new InputArgument(self::STATUS, InputArgument::OPTIONAL, "Status [pending,processing,completed,closed]"),
            new InputArgument(self::ID, InputArgument::OPTIONAL, "Order Entity ID"),
            new InputOption(self::TEST, "-t", InputOption::VALUE_NONE, "test")
        ]);
        parent::configure();
    }

    public function getSalesOrderCollection(array $filters = [])
    {

        $collection = $this->collectionFactory->create()
            ->addAttributeToSelect('*');

        foreach ($filters as $field => $condition) {

            $collection->addFieldToFilter($field, $condition);
        }

        return $collection;
    }
}

